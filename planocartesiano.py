print("Coordenadas en el mapa cartesiano")
print("Por Rafael Aquino")
x = int(input("X: "))
y = int(input("Y: "))
sw = False
if x == 0 and y == 0:
  print("Este es el origen!. (0,0)")
  sw = True

if (x == 0 or y == 0) and (sw == False):
  print("Una de las coordenadas es igual a cero!")
  
if x > 0 and y > 0:
  print("I")

if x < 0 and y > 0:
  print("II")

if x < 0 and y < 0:
  print("III")
  
if x > 0 and y < 0:
  print("IV")