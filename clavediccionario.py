print('Obtener llaves de un diccionario')
print('Por Rafael Aquino \n')

midiccionario = {
  'nombre' : 'Rafael',
  'edad'   :  50 
}

print('Dicionario')
print(midiccionario)

print('LLaves del diccionario')
llaves = midiccionario.keys()
print(llaves)
print('Las llaves son ', tuple(llaves))
#Otra forma
print()
print('LLaves del diccionario')
for key in midiccionario:
  print(key)