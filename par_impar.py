print("Determinar si un numero generado aleatoriamente es par o impar")
print("Elaborado por Rafael Aquino")

import random

x = random.randint(1,100)

if ((-1)**x) == 1:
    print(f'{x} es Par')
else:
    print(f'{x} es Impar')