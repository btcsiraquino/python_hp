print('Por Rafael Aquino')
print('Año Bisiesto')

anno = int(input('Introduzca el Año: '))

if ((anno % 400 == 0) or (anno % 100 != 0)) and (anno % 4 == 0):
  print('¡Año bisiesto!')
else:
  print('¡Año ordinario!')