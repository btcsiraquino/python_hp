"""
Contador de caracteres

Define una función que nos permita conocer la cantidad de caracteres que pose un string. 
-No contar los espacios en blanco
Por Rafael Aquino
20221117
"""

def string_counte(palabra):
  cont = 0
  for i in palabra:
    if i != " ":
      cont +=1
  return cont
    

p = input("introduzca palabra: ")

contador = string_counte(p)
print(f' La palabra << {p} >> tiene {contador} caracteres')