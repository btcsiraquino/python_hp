print(' Ordenar de forma descendente tres numeros')
print(' Por Rafael Aquino ' )


print("Programa de ordena de forma descendente tres numeros")
n1 = int(input("Introduce tu primer número: "))
n2 = int(input("Introduce tu segundo número: "))
n3 = int(input("Introduce tu tercer número: "))
mayor = max(n1,n2,n3)
print("El mayor es: ", mayor)
menor = min(n1,n2,n3)
print("El menor es: ",menor)
medio = (n1+n2+n3) - (mayor + menor)
print("El del medio es:" , medio)

print("El orden es: ", mayor," - ", medio," - ", menor)


print("Otra Forma")
n10= int(input("Introduce tu primer número: "))
mayor = n10
n20 = int(input("Introduce tu segundo número: "))
menor = n20
if mayor <= menor:
  aux = mayor
  mayor = menor
  menor = aux
print("mayor es ", mayor)
print("menor es ", menor)
  
n30 = int(input("Introduce tu tercer número: "))
medio = n30
if (medio > mayor):
  aux2 = medio
  medio = mayor
  mayor = aux2
if (medio < menor):
  aux3 = medio
  medio = menor
  menor = aux3
print("el medio es ",medio)
print("El orden es: ", mayor,medio,menor)